
package January28TasksinClass;

public class ISBNValidation {
    
    static boolean isISBNValid(String isbn)
    {
        String digitToInt[]={"0","1","2","3","4","5","6","7","8","9","X"};
        if(isbn.length()!=10)
        {
            throw new IllegalArgumentException("ISBN must be 10 characters long");
        }
        int isbnAsInts[]= new int[10];
        for(int i=0;i<isbn.length();i++)
        {
            char c=isbn.charAt(i);
            int n=0;
            //find the index of character c in the array
            for(;n<digitToInt.length;n++)
            {
             //  if(c==digitToInt[n])
                {
                    break;
                }
            }
            if(n==digitToInt.length)
            {
                throw new IllegalArgumentException("Charcater" + c+"is valid");
            }    
            isbnAsInts[i]=n;
        }
        int sum=0;
        for(int i=0;i<isbnAsInts.length;i++)
        {
            int mul=10-i;
            sum+=isbnAsInts[i]*mul;
        }
        
        return true;
    }

    public static void main(String[] args) {
      
    }
    
}
