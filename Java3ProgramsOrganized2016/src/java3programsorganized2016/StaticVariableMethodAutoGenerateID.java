package java3programsorganized2016;

 class Person1 {
    private static int instancesCount;
    
    private int id;

    String name;
    int age;
    
    Person1() {
        instancesCount++;
        id = instancesCount;
    }
    
    int getId() {
        return id;
    }
    
    static int getInstancesCount() {
        return instancesCount;
    }
    
}

public class StaticVariableMethodAutoGenerateID {

    public static void main(String[] args) {
        Person1 p1 = new Person1();
        Person1 p2 = new Person1();
        Person1 p3 = new Person1();
        System.out.println("Person instances count: " + Person1.getInstancesCount() );
        System.out.printf("IDs p1:%d, p2:%d, p3:%d\n",
                p1.getId(), p2.getId(), p3.getId());
    }
    
}
