package numberfrequency;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;



public class HaspMapArraylistNumberFrequency {

//     static ArrayList< ArrayList<String> > ArrayListOfArrayList;    
//     static HashMap<String, ArrayList<Integer> > HashMapOfArrayList;
    
    final static String INPUT_FILE = "numbers.txt";
    
    static ArrayList<Integer> intList = new ArrayList<>();
    
    static HashMap<Integer,Integer> frequencyMap = new HashMap<>();
            
    static void registerOccurence(int number) {
        if (!frequencyMap.containsKey(number)) {
            frequencyMap.put(number, 1);
        } else {
            int count = frequencyMap.get(number);
            frequencyMap.put(number, count + 1);
        }
    }
    
    public static void main(String[] args) throws IOException {
        File file = new File(INPUT_FILE);
        Scanner fileInput = new Scanner(file);
        while (fileInput.hasNextInt()) {
            intList.add(fileInput.nextInt());
        }
        //
        for (int num : intList) {
            registerOccurence(num);
        }
        //
        for (int key : frequencyMap.keySet()) {
            int value = frequencyMap.get(key);
            System.out.printf("Int %d occured %d times\n", key, value);
        }
        // TODO: What would we need to do to sort output by frequencies?
    }
    
}
