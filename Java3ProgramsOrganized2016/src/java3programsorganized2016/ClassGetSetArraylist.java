/* TASK 1 Create project Review2
Create class Person with following fields:
- name String
- int age
Make fields private and add getters/setters to encapsulate the fields.
Add constructor with two parameters name, age that initializes fields of this class.
Modify setters to verify that:- name is at least 2 characters long - age is between 0 and 150
If the requirement is broken throw IllegalArgumentException*/
package java3programsorganized2016;



public class ClassGetSetArraylist 
{
    public static void main(String[] args) 
    {
        Person p = new Person("J.J. Abrahms", 40);
        System.out.println(p.getName()+":"+p.getAge());
                
    }    
}
class Person
{
private String Name;
private int Age;

    public String getName()
    {
        return Name;
    }
    public void setName(String name)
    {
        if (name.length() < 2) {
            throw new IllegalArgumentException("Name too short");
        }
        this.Name = name;
    }
    public int getAge()
    {
        return Age;
    }
     public void setAge(int age) {
        if ((age < 0) || (age > 150)) {
            throw new IllegalArgumentException("Age must be between 0-150");
        }
        this.Age = age;
    }
    Person(String name, int age) 
    {
        setName(name);
        setAge(age);
    }
    
}
