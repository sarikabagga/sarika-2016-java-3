package BookExercises;

import java.util.Scanner;

public class BookTriangle4 {
    static   Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
 
        
        int N = sc.nextInt();

        // loop N times, one for each row
        for (int i = N; i < 0; i++) {

            // print j periods
            for (int j = 0; j < i; j++)
                System.out.print(". ");

            // print N-i asterisks
            for (int j = 0; j < N-i; j++)
                System.out.print("* ");

            // print a new line
            System.out.println();
        }
    }
    }
    

