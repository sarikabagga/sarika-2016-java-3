
package BookExercises;

import java.util.Scanner;

public class BookFibonacci1 
{
 public static void main(String[] args) {
    System.out.println("Largest integer Fibonacci number: " + maxFibonacci());
}

public static int maxFibonacci() {
    int n = Integer.MAX_VALUE;
    int fib = 1;
    int temp = 0;

    for (int i = 2; i < n; i++) {
        int last = fib;
        fib += temp;
        if (fib < 0) return last; //overflow, we are done
        temp = last;
    }
    return 0; //unreachable
}
    }


