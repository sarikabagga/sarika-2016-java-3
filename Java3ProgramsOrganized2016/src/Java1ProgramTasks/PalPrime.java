/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Java1ProgramTasks;

/**
 *
 * @author ordicell
 */
/**
* The class PalPrime prints all the Prime Palindrome numbers in the given range
* @author : www.javaforschool.com
* @Program Type : BlueJ Program - Java
* @Question Year : ISC 2012 Question 1
*/
 
import java.io.*;
import java.math.BigInteger;
class PalPrime
{
BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
 
/* Function isPrime( ) returns 'true' when the number 'x' is Prime and 'false' if it is not. */
boolean isPrime(BigInteger x)
{
int count=0;
 for (BigInteger i = BigInteger.valueOf(1);
            i.compareTo(x) <= 0;
            i = i.add(BigInteger.ONE))
{
if(x.mod(i)==BigInteger.ZERO)
count++;
}
if(count==2)
return true;
else
return false;
}
 
/*Function isPalin( ) returns 'true' when 'x' is a Palindrome and 'false' if it is not.*/
boolean isPalin(BigInteger x)
{
BigInteger rev=BigInteger.ZERO;
BigInteger dig;
BigInteger copy=x;
while( x.compareTo(BigInteger.ZERO) > 0)
{

        dig=x.mod(BigInteger.TEN);
rev=(rev.multiply(BigInteger.TEN)).add(dig);
x = x.divide(BigInteger.TEN);
}
int res=rev.compareTo(copy);
if(res==0)
return true;
else
return false;
}
 
/* Function showPalPrime( ) accepts the lower and upper limit, and prints all the PalPrime numbers
in between that range by sending each numbers in the range to both the functions isPrime( ) and isPalin( ) */
 
public void showPalPrime() throws IOException
{
int m,n;
int c=0;
//System.out.print("Enter the Lower Limit (m) = ");
//m=Integer.parseInt(br.readLine());
//System.out.print("Enter the Upper Limit (n) = ");
//n=Integer.parseInt(br.readLine());
 
//if(m>=n || m>=3000 || n>=3000) // Checking the range of Limits as given in the question
System.out.println("OUT OF RANGE");
//else
{
System.out.println("The Prime Palindrome integers are:");
 
/* The below for loop generates every number starting from 'm' till 'n' and sends it 
to both functions isPalin() and isPrime(), to check whether they are both Palindrome and 
prime or not. If yes, then they are printed. */
     for (BigInteger i = BigInteger.valueOf(2);
            i.compareTo(BigInteger.valueOf(100000L)) < 0;
            i = i.add(BigInteger.ONE))
//for(int i=m; i<=n; i++)
{
if(isPrime(i)==true && isPalin(i)==true)
{
if(c==0)
System.out.print(i); 
 
/*The above line is printing the first PalPrime number in order to maintain the sequence 
of giving a comma ',' before every next PalPrime number, as is given in the Sample Output.*/
 
else
System.out.print(", "+i);
c++; //Counting the number of PalPrime numbers by incrementing the counter
}
}
System.out.println("Frequency of Prime Palindrome integers: "+c);
}
}
 
/* The main method creates an object of PalPrime Class and calls the function showPalPrime( ) */
public static void main(String args[])throws IOException
{
PalPrime ob=new PalPrime();
ob.showPalPrime();
}
}
