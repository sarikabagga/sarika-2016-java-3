package review2;

class Person {

    private String name;
    private int age;

    Person(String name, int age) {
        setName(name);
        setAge(age);
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.length() < 2) {
            throw new IllegalArgumentException("Name too short");
        }
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if ((age < 0) || (age > 150)) {
            throw new IllegalArgumentException("Age must be between 0-150");
        }
        this.age = age;
    }
}

public class Review2 {

    public static void main(String[] args) {
        Person p = new Person("J.J. Abrahms", 40);
    }
    
}
