package FilePrgsBySir;

import January25Tasksatclass.TaskArray1File1;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileWriteRandomNo5 {

    static Scanner input = new Scanner(System.in);

    static int inputInt() {
        for (;;) {
            try {
                return input.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Please enter integer value");
                input.nextLine();
            }
        }

    }

    public static void main(String[] args) {

        // get input from user
        System.out.print("Minumum? ");
        int min = inputInt();
        System.out.print("Maximum? ");
        int max = inputInt();
        // generate
        Random random = new Random();
        int[] intArray = new int[10];
        for (int i = 0; i < intArray.length; i++) {
            int value = random.nextInt(max - min + 1) + min;
            intArray[i] = value;
        }
        // sort and display
        Arrays.sort(intArray);
        System.out.print("Values: ");
        try {
            PrintWriter pw = new PrintWriter("outputFileWriteRandomNo.txt");
            for (int i = 0; i < intArray.length; i++) {
                int value = intArray[i];
                pw.println(value);
                System.out.printf("%d%s", value, i == intArray.length - 1 ? "\n" : ", ");
            }
            pw.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TaskArray1File1.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
