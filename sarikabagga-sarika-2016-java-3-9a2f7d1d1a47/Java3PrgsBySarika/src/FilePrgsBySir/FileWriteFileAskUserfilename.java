/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FilePrgsBySir;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author ordicell
 */
public class FileWriteFileAskUserfilename {

    public static void main(String[] args) {
        System.out.println("Type your friends names: ");
        ArrayList<String> listFriends = new ArrayList<String>();
        Scanner sc = new Scanner(System.in);

        try {

            while (true) {
                String name = sc.nextLine();
                if (name.equals("")) {
                    break;
                }
                listFriends.add(name);
            }

            System.out.println("Enter file name: ");
            String fileName = sc.nextLine() + ".txt";

            File file = new File(fileName);
            PrintWriter wr = new PrintWriter(file);
            for (String fr : listFriends) {
                wr.println(fr);
            }
            wr.close();
            System.out.println("File saved successfully.");
        } catch (IOException e) {
            System.out.println("Enter valid name.");
        }
        System.out.printf("%5d: ", 100);
    }
}
