
package FilePrgsBySir;

import java.io.IOException;
import java.io.PrintWriter;

public class FileWrite1 {

    public static void main(String[] args) {
   // Part 1: write one line to text file
        try 
        {
            PrintWriter pw = new PrintWriter("twice1.txt");
            pw.println("I hope this works");
            pw.println("Line 2222");
            pw.println("Third 33333333");
            pw.close();
        } 
        catch (IOException e)
        {
            System.out.println("Error writing to file");
        }
    }
    
}
