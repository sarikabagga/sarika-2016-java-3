package Quiz1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Java3Quiz1 {

    final static String INPUT_FILE = "teams.txt";
    static ArrayList<Team> teamList = new ArrayList<>();

    static ArrayList<Integer> list = new ArrayList<>();
    static ArrayList<Integer> removed = new ArrayList<>();
    static int min, max,noofwingames,noofwingamesper,nooflostgames,nooflostgamesper,nooflostgame;
    static float largest;
    static String winnerTeam,winnerTeamper;
    static void readTeamsResults() throws IOException {
        File file = new File(INPUT_FILE);
        try {
            Scanner fileInput = new Scanner(file);
            while (fileInput.hasNextLine()) {
                String teamname = fileInput.nextLine();
                int nowingame = fileInput.nextInt();
                int nolostgame = fileInput.nextInt();
                fileInput.nextLine(); // consume \n clear out the/n character from buffer
                Team t = new Team(teamname, nowingame, nolostgame);
                teamList.add(t);
            }
            fileInput.close();
            System.out.println("File saves scucessfully");
        } catch (IOException e) {
            System.out.println("Error reading from file");
        }
    }

    static void displayResults() throws IOException {

        for (Team t : teamList) {
            if (t.gamesWon > max) {
                max = t.gamesWon;
                winnerTeam = t.name;
                noofwingames = t.gamesWon;
                nooflostgames = t.gamesLost;

            }

        }
        for (Team t : teamList) {

            float value1 = (float) (t.gamesWon) / (t.gamesWon + t.gamesLost) * 100;

            if ((value1 > largest)) {

                largest = value1;
                winnerTeamper = t.name;
                noofwingamesper = t.gamesWon;
                nooflostgamesper = t.gamesLost;
            }

        }
     //   float old = -3.04299553323;  
//float new = [[NSString stringWithFormat:@"%.2f",largest]floatValue];.
        System.out.println("Team" + " " + winnerTeam + " won most games:" + noofwingames);
        System.out.println("Team" + " " + winnerTeamper + " won largest % of games:" + largest);
        System.out.printf("Team %s   %.2f\n", winnerTeamper, largest);
    }

    public static void main(String[] args) {
        try {
            readTeamsResults();
            displayResults();
        } catch (IOException e) {
            System.err.println("File access error");
        }
    }
}

class Team {

    String name;
    int gamesWon;
    int gamesLost;

    Team(String name, int gamesWon, int gamesLost) {
        this.name = name;
        this.gamesWon = gamesWon;
        this.gamesLost = gamesLost;
    }
}
