package MagdyPrograms;


import java.util.Scanner;

/**
 *
 * @author Mrbig
 */
public class Exercises1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int num1[] = new int[5];
        int num2[]= {0,0,0,0,0};
        Scanner input = new Scanner(System.in);

        for (int i=0 ; i < 5 ; i++)
        {
            do
            {
                System.out.println("Enter number between 10 and 100:");
                num1[i] = Integer.parseInt(input.nextLine());
            }
            while ((num1[i] <10) || (num1[i] >100));
            System.out.println("The numbers are: ");
            System.out.println("******************");

            boolean different = true;

            for (int j = 0 ; j < i ; j++)
            {
                    if (num2[j]==num1[i])
                    {
                       different=false;
                       break;
                    }
            }
                if (different==true)
                    num2[i]=num1[i];
                for (int j=0 ; j<=i ; j++)
                {
                    if(num2[j] !=0)
                        System.out.println(num2[j]);
                }

            System.out.println("******************");
        }

    }
}
