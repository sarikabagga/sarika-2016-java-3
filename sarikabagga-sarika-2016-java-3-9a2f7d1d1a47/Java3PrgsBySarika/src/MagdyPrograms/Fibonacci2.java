package MagdyPrograms;

import java.util.Scanner;

public class Fibonacci2 {
    public static double fib(int n) {
        if (n <= 1) return n;
        else return fib(n-1) + fib(n-2);
    }

public static void main(String[] args) {
Scanner input = new Scanner(System.in);
int n;
n = input.nextInt();
System.out.printf("The Fibonacci of number %d is : %n 0 " , n );
        for (int i = 1; i <= n; i++)
	{
      	System.out.printf( " , %.0f " , fib(i) );
	}
	System.out.println();
	System.out.println("The largest Double Number is " + Double.MAX_VALUE);

    }

}
