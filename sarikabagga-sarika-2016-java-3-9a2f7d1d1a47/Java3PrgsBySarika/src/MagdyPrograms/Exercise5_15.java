package MagdyPrograms;

public class Exercise5_15{
public static void main(String args[]){

System.out.println("(a)");
for (int i=1 ; i<=10 ; i++){
	for(int j=1 ; j <=i ; j++){
		System.out.print("* ");
	}
	System.out.println();
}

System.out.println("(b)");
for (int i=10 ; i>=1 ; i--){
	for(int j=1 ; j <=i ; j++){
		System.out.print("* ");
	}
	System.out.println();
}

System.out.println("(c)");
for (int i=10 ; i>=1 ; i--){
	for(int sp=1 ; sp <= 10-i ; sp++)
	System.out.print("  ");
	for(int j=1 ; j <=i ; j++){
		System.out.print("* ");
	}
	System.out.println();
}

System.out.println("(d)");
for (int i=1 ; i<=10 ; i++){
	for(int sp=1 ; sp <= 10-i ; sp++)
	System.out.print("  ");
	for(int j=1 ; j <=i ; j++){
		System.out.print("* ");
	}
	System.out.println();
}

}

}