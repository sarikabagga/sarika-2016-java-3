/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package January25Tasksatclass;

import static January25Tasksatclass.TaskAdditionArray2.sc;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TaskArray1File1 {

    static Scanner input = new Scanner(System.in);

    static int inputInt() {
        for (;;) {
            try {
                return input.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Please enter integer value");
                input.nextLine();
            }
        }

    }

    public static void main(String[] args) {

        Random rn = new Random();
        System.out.print("Ener min:");
        //int min = input.nextInt();
        int min = inputInt();

        System.out.print("Ener max:");
        //int max = input.nextInt();
        int max = inputInt();
        /*
         //one way
         for (int i = 0; i < 10; i++) {
         int answer = rn.nextInt((max - min) + 1) + min;
         //System.out.println(answer);
         System.out.printf("%d,", answer);
        
         } */
        Random random = new Random();
        int[] intArray = new int[10];
        //  int[] array2 = new int[10];
        int value;

        for (int i = 0; i < intArray.length; i++) {
            value = random.nextInt((max - min) + 1) + min;

            intArray[i] = value;

        }

        Arrays.sort(intArray);
        // for(int i=0;i<array2.length;i++)
        for (int value1 : intArray) //System.out.printf("%d,", array2[i]);
        {
            System.out.print(value1 + " ,");
        }

        try {
            PrintWriter pw = new PrintWriter("output.txt");
            for (int i = 0; i < intArray.length; i++) {
                int value1 = intArray[i];

                pw.println(value1);
                System.out.printf("%d%s", value1, i == intArray.length - 1 ? "\n" : ",");
              
            }
            pw.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TaskArray1File1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
