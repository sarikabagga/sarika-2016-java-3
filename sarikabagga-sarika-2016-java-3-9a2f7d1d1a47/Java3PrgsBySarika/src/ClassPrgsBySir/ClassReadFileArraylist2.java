/*In the main() method you will:
- open file 'data.txt' for reading
- in a while() loop you will read pairs of lines of the file
- at every iteration you will instantiate new object  of class Person and give it values of name and age
you read from the two lines of the file
- close the file
Then write a for-each loop that will go over all items in personList and print out Name/Age on the screen, e.g:
Jimmy is 34
Eva is 22 */
package ClassPrgsBySir;

import java3prgsbysarika.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class ClassReadFileArraylist2 
{
    static ArrayList<Person> personList = new ArrayList<>();
    
    public static void main(String[] args) 
    {
         File file = new File("data.txt");
         try
             {
            Scanner fileInput = new Scanner(file);
            while (fileInput.hasNextLine()) 
            {
                String name = fileInput.nextLine();
                int age = fileInput.nextInt();
                fileInput.nextLine(); // consume \n
                Person p = new Person(name, age);
                personList.add(p);
            }
            fileInput.close();
        } 
         catch (IOException e)
         {
            System.out.println("Error reading from file");
        }
        // TODO: use for-each loop to print all personList object data
        for (Person p : personList) 
        {
            System.out.printf("%s is %d\n", p.getName(), p.getAge());
        }
        
        
    }    
}
