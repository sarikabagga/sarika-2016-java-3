/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NumberFrequency;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

public class Hasconsectiveorder {

    final static String INPUT_FILE = "numbers.txt";

    static ArrayList<Integer> intList = new ArrayList<>();

   // static HashMap<Integer,Integer> frequencyMap = new HashMap<>();
    static HashMap<Integer, Integer> frequencyMap = new HashMap<Integer, Integer>();

   

    public static void main(String[] args) throws IOException {
         Scanner in = new Scanner(System.in);
      System.out.println("Please enter a list of numbers: ");
       
     for (int i = 0; i < 10; i++)
      {
         int input = in.nextInt();

        intList.add(input);
      }
       Collections.sort(intList);
       
        for (int num : intList) {
            //  registerOccurence(num);
            System.out.print(num + ",");
            Integer f = frequencyMap.get(num);
            if (f == null) {
                frequencyMap.put(num, 1);
            } else {
                frequencyMap.put(num, f + 1);
            }
            //        frequencyMap.put(num,f+1);
        }
       
        //Hash map key does not have to be int it can be string
        
        //Using Sir Method
          System.out.println("Number | Occurences \n" +"-------+------------");
        for(int key:frequencyMap.keySet())
        {
            int value=frequencyMap.get(key);
           // System.out.printf("In %d occured %d times\n" ,key, value);
          
            System.out.printf("%d | %d \n" ,key, value);
            
            if(value>=4)
                System.out.println("true");
        }
        //TODO sort by frequency.
        
    }

}
