package Quiz2PrgsJan27;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class NumberFrequency {

    static ArrayList<Integer> intList = new ArrayList<>();
    static ArrayList<Integer> occurences = new ArrayList<>();

    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("frequency.txt");
        try {
            Scanner fileInput = new Scanner(file);
            while (fileInput.hasNextInt()) 
            {
                int line = fileInput.nextInt();
                System.out.println(line);
                intList.add(line);
            }
            fileInput.close();

            System.out.println("----------------------");

                for (int i = 0; i < intList.size(); i++)
                {
                    int value = intList.get(i);
                    int occurrences = Collections.frequency(intList, intList.get(i));
                    //System.out.println(value);
                     //System.out.println("----------------------");
                    System.out.println(occurrences);
                }

            }
            catch (IOException e)
        {
            System.out.println("Error reading from file");
        }
        
    
        }}
