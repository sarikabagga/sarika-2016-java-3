package Quiz2PrgsJan27;

import java.util.Scanner;

public class Conversion {
static String dec2hex(int dec)
{
    //char hex[]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
    String [] hex={"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"};
    String str2=""; 
    while(dec>0)
     {
      int  rem=dec%16; 
      str2=hex[rem]+str2; 
       dec=dec/16;
     }
    return str2;
    
}

static String dec2any(int dec,int base)
{
    if((base<2)||(base>16))
    {
        //throw new IllegalArgumentException();
        IllegalArgumentException e=new IllegalArgumentException("Base must be netween 2 and 16");
        throw e;
    }
    //char hex[]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
    String [] tohex={"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"};
    String result=""; 
    while(dec>0)
     {
      int  lastDigit=dec%base; 
      String digit=tohex[lastDigit]; 
      result=digit+result;
       dec=dec/base;
     }
    return result;
    
}
static String dec2bin(int dec)
{
    //return dec2any(dec,2);
    //return "";
    int binary[] = new int[40];
     int index = 0;
     String binaryout="";
     while(dec > 0){
       binary[index++] = dec%2;
       dec = dec/2;
     }
     for(int i = index-1;i >= 0;i--){
        binaryout=binaryout+binary[i];
     }
    return binaryout;
}
static String dec2oct(int dec)
{
      //return dec2any(dec,8);
    //return "";
       int rem;
    // For storing result
    String str=""; 
    // Digits in Octal number system
    char dig[]={'0','1','2','3','4','5','6','7'};
 
    while(dec>0)
    {
       rem=dec%8; 
       str=dig[rem]+str; 
       dec=dec/8;
    }    
    return str;
}

    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);
        System.out.println("Enter an positive integer");
        int value=input.nextInt();
        try{
        System.out.println("In hexadecimal:"+dec2hex(value));
        }
        catch(IllegalArgumentException e)
        {
e.printStackTrace(System.out); //error output
//
        }
        try
        {
        System.out.println("In hexadecimal:"+dec2any(value,17));
        }
        catch(IllegalArgumentException e)

        {
            //
            e.printStackTrace();
        }
         System.out.println("In Binary:"+dec2bin(value));
         System.out.println("In Binary:"+dec2any(value,2));
         System.out.println("In Octal:"+dec2oct(value));
         System.out.println("In Octal:"+dec2any(value,8));
    }
    
}
