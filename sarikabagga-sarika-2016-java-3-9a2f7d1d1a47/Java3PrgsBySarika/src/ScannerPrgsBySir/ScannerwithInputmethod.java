package ScannerPrgsBySir;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class ScannerwithInputmethod {

    static Scanner input = new Scanner(System.in);

    static int inputInt() {
        for (;;) {
            try {
                return input.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Pleas enter an integer value.");
                input.nextLine(); // consume the invalid user input
            }
        }
    }

    public static void main(String[] args) {
        // get input from user

        System.out.print("Minumum? ");
        int min = inputInt();
        System.out.print("Maximum? ");
        int max = inputInt();
        // generate
        Random random = new Random();
        int[] intArray = new int[10];
        for (int i = 0; i < intArray.length; i++) {
            int value = random.nextInt(max - min + 1) + min;
            intArray[i] = value;
        }
        // sort and display
        Arrays.sort(intArray);
        System.out.print("Values: ");
        for (int i = 0; i < intArray.length; i++) {
            int value = intArray[i];
            System.out.printf("%d%s", value, i == intArray.length - 1 ? "\n" : ", ");
        }
    }
}
