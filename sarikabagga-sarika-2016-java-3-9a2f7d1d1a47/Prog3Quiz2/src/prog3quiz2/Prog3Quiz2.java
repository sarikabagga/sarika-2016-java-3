package prog3quiz2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Prog3Quiz2 {

    final static String INPUT_FILE = "points.txt";
    static ArrayList<Point> ptList = new ArrayList<>();
    static ArrayList<Double> ptNumbers = new ArrayList<>();

    static void readPointValues() throws IOException {
        File file = new File(INPUT_FILE);
        Scanner inputFile = new Scanner(file);
        while (inputFile.hasNextDouble()) {
            Double x = inputFile.nextDouble();
            Double y = inputFile.nextDouble();
            inputFile.nextLine(); // clear out the \n character from buffer
            Point pt = new Point(x, y);
            ptList.add(pt);         
        }
        inputFile.close();
        //System.out.printf("File %s read successfully.\n", INPUT_FILE);
    }

    static void readNumbers() throws IOException {        
        File file = new File(INPUT_FILE);
        Scanner inputFile = new Scanner(file);
        while (inputFile.hasNextDouble()) {
            Double num = inputFile.nextDouble();            
            inputFile.nextLine(); // clear out the \n character from buffer            
            ptNumbers.add(num);           
        }
        inputFile.close();
        //System.out.printf("File %s read successfully.\n", INPUT_FILE);
    }

    static void displayResultsPoint() {
        for (Point p : ptList) {           
            double distance = Math.sqrt(Math.pow((p.y - p.x), 2) + Math.pow((p.y - p.x), 2));
            System.out.printf("Distance from %.2f and %.2f  is %.2f\n",p.x,p.y ,distance);
        }

         
    }
//-------------Part 2   ================================
    static double getMean() {
        double sum = 0.0;
        int sizelist = ptNumbers.size();
        for (double a : ptNumbers) 
        {
            sum += a;
        }
        return sum / sizelist;
    }

    static double getVariance() {
        double mean = getMean();
        double temp = 0;
        int sizelist = ptNumbers.size();
        for (double a : ptNumbers) {
            temp += (mean - a) * (mean - a);
        }
        return temp / sizelist;
    }

    static void findSecondLargestNum() {
        Double secondlargest = 0.0;
        Double largest = 0.0;
        for (int i = 0; i < ptNumbers.size(); i++) {
            //System.out.println(ptNumbers.get(i));
            if (largest < ptNumbers.get(i)) {
                secondlargest = largest;
                largest = ptNumbers.get(i);
            }
            if (secondlargest < ptNumbers.get(i) && largest != ptNumbers.get(i)) {
                secondlargest = ptNumbers.get(i);
            }
        }

        System.out.printf("Second largest number  is %.2f\n", secondlargest);

    }

    static void findSecondSmallestNum() {       
        Double smallest = Double.MAX_VALUE;
        Double secondSmallest = Double.MAX_VALUE;

        for (int i = 0; i < ptNumbers.size(); i++) {
            if (smallest > ptNumbers.get(i)) {
                smallest = ptNumbers.get(i);
            }

        }
        for (int i = 0; i < ptNumbers.size(); i++) {
            if (secondSmallest > ptNumbers.get(i) && ptNumbers.get(i) > smallest) {
                secondSmallest = ptNumbers.get(i);
            }

        }

        //System.out.println("The smallest element is: " + smallest + "\n"+  "The second smallest element is: " + secondSmallest);
        System.out.printf("Second smallest number  is %.2f\n", secondSmallest);
    }

    static void greaterthanaverage() throws FileNotFoundException {
        double sum = 0.0;
        PrintWriter pw = new PrintWriter("output.txt");
        for (Double val : ptNumbers) {
            sum += val;
        }

        for (Double val1 : ptNumbers) {
            if (val1 > sum / ptNumbers.size()) {
                //System.out.println(val1);

                pw.println(val1);
            }
        }
        pw.close();
    }

    static void displayResultNumbers() {

        double Variance = getVariance();
        System.out.printf("variance  is %.2f\n", Variance);
         //Part2
            findSecondLargestNum();
            findSecondSmallestNum();
    }

    public static void main(String[] args) {
        try {
            //part-1
            readPointValues();
            displayResultsPoint();
            //part-2
            readNumbers();
            displayResultNumbers();           
            //Part 3
            greaterthanaverage();            

        } catch (IOException e) {
            System.out.println("Internal error");
        }
    }

}

class Point {

    double x;
    double y;

    Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

}
