package prog3quiz1solution;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

class Team {
    
    String name;
    int gamesWon;
    int gamesLost;
    
    Team(String name, int gamesWon, int gamesLost) {
        this.name = name;
        this.gamesWon = gamesWon;
        this.gamesLost = gamesLost;
    }
}

public class Prog3Quiz1Solution {

    final static String INPUT_FILE = "teams.txt";

    static void readTeamsResults() throws IOException {
        // open file 'teams.txt' for reading
        // read the data and store in teamList
        File file = new File(INPUT_FILE);
        Scanner inputFile = new Scanner(file);
        
        while (inputFile.hasNextLine()) {
            
            String name = inputFile.nextLine();
            int gamesWon = inputFile.nextInt();
            int gamesLost = inputFile.nextInt();
            inputFile.nextLine(); // clear out the \n character from buffer
            
            Team team = new Team(name, gamesWon, gamesLost);
            
            teamList.add(team);
        }
        inputFile.close();
        System.out.printf("File %s read successfully.\n", INPUT_FILE);
    }

    static void displayResults() {
        // 1. find team that won largest number
        // of games and display it        
        String teamLargestNumberWon = "";
        int largestNumberWon = -1;
        for (Team team: teamList) {
            if (team.gamesWon > largestNumberWon) {
                teamLargestNumberWon = team.name;
                largestNumberWon = team.gamesWon;
            }
        }
        System.out.printf("Team %s won the most games: %d\n",
                teamLargestNumberWon, largestNumberWon);
        // 2. find team that won largest % of games
        // and display it
        String teamLargestPercentageWon = "";
        double largestPercentageWon = -1;
        for (Team team: teamList) {
            double percentage = (double)team.gamesWon / (team.gamesWon + team.gamesLost);
            if (percentage > largestPercentageWon) {
                teamLargestPercentageWon = team.name;
                largestPercentageWon = percentage;
            }
        }
        System.out.printf("Team %s won the largest %% of games: %.2f%%\n",
                teamLargestPercentageWon, largestPercentageWon*100);
    }

    static ArrayList<Team> teamList = new ArrayList<>();

    public static void main(String[] args) {
        try {
            readTeamsResults();
            displayResults();
        } catch (IOException e) {
            System.out.println("Internal error");
        }
    }

}
