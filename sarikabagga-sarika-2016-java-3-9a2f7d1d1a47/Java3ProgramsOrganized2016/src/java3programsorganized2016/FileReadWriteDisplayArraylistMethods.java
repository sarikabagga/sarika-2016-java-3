package java3programsorganized2016;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class FileReadWriteDisplayArraylistMethods {

    static ArrayList<Integer> list = new ArrayList<>();
    static ArrayList<Integer> removed = new ArrayList<>();
    static int min, max;

    static void readInput() throws IOException {
        File file = new File("inputminmaxcompute.txt");
        try {
            Scanner fileInput = new Scanner(file);
            while (fileInput.hasNextInt()) {
                int filevalue = fileInput.nextInt();
                list.add(filevalue);
            }
            fileInput.close();
        } catch (IOException e) {
            System.out.println("Error reading from file");
        }
    }

    static void writeOutput() throws IOException {

        File file = new File("outputfilter.txt");
        File fileRemoved = new File("removedfilter.txt");
        PrintWriter pw = new PrintWriter(file);
        PrintWriter wrRemove = new PrintWriter(fileRemoved);
        pw.println("Minimum Value:" + min);
        pw.println("Maximum Value:" + max);
        for (int value : list) //System.out.printf("%d,", array2[i]);
        {
            pw.println(value);
        }
        for (int num : removed) {
            wrRemove.println(num);
        }
        pw.close();
        wrRemove.close();
    }

    static void computeResult() {
        max = Integer.MIN_VALUE;
        min = Integer.MAX_VALUE;

          //  min=Collections.min(list);
        for (int value : list) {
            if (value < min) {
                min = value;
            }
            if (value > max) {
                max = value;
            }
        }

        /* my way     for (int i = 0; i < list.size(); i++) {
         if (list.get(i) < 0) {
         // intArray.remove(i);
         list.remove(list.get(i));
         }
         if (list.get(i) % 3 == 0) {
         list.remove(list.get(i));
         }
            
         }*/
        ArrayList<Integer> newlist = new ArrayList<>();
        for (int value : list) {
            if (value % 3 == 0) {
                removed.add(value);
                continue;
            }
            if (value < 0) {
                removed.add(value);
                continue;
            }
            newlist.add(value);
        }
        list = newlist;
        Collections.sort(removed);
         //if for arraylist
        //review 3 in the quiz
        //for loop version of it
        /* sir another way
         for (int i = 0; i < list.size(); i++) {
         int value = list.get(i);
         if ((value % 3 == 0) && (value < 0)) {
         list.remove(i);
         i--;
         }
         }*/

    }

    public static void main(String[] args) {
        try {
            readInput();
            computeResult();
            writeOutput();
        } catch (IOException e) {
            System.err.println("File access error");
        }
        // TODO code application logic here
    }
}
