package Java1ProgramTasks;

import java.util.Arrays;
import java.util.Scanner;

public class PrgUserInputCalMedianAvgSd {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        double []array = new double[9];
        for (int i=0; i<array.length; i++) {
            System.out.print("Enter an integer number: ");
            array[i] = input.nextDouble();
        }
        // compute the average and display it
        double sum = 0;
        for (double n : array) {
            sum += n;
        }
        double average = sum / array.length;
        System.out.printf("Average is %.4f\n", average);
        //  find the median and display
        Arrays.sort(array);
        double median = array[4];
        System.out.printf("Median is %.4f\n", median);
        
        // compute and print out standard deviation
        double sum2 = 0;
        for (double n : array) {
            sum2 += (n-median)*(n-median);
        }
        double stdDev = Math.sqrt(sum2/array.length);
        System.out.printf("Standard deviation is %.4f\n", stdDev);

    }
    
}
