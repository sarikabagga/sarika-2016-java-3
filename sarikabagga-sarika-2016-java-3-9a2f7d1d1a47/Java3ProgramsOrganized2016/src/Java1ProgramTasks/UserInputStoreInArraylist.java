/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Java1ProgramTasks;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

public class UserInputStoreInArraylist {    
    static ArrayList<Integer> intList = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        System.out.println("Please enter a list of numbers: ");

        for (int i = 0; i < 10; i++) {
            int input = in.nextInt();

            intList.add(input);
        }
        Collections.sort(intList);

        for (int num : intList) {
            //  registerOccurence(num);
            System.out.print(num + ",");           
        }
       

    }

}
