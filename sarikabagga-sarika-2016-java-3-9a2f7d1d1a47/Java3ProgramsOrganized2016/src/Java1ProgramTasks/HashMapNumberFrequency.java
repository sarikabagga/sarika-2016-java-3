/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Java1ProgramTasks;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class HashMapNumberFrequency {

    final static String INPUT_FILE = "numbers.txt";

    static ArrayList<Integer> intList = new ArrayList<>();

    // static HashMap<Integer,Integer> frequencyMap = new HashMap<>();
    static HashMap<Integer, Integer> frequencyMap = new HashMap<Integer, Integer>();

    static void registerOccurence(int number) {
        if (!frequencyMap.containsKey(number)) {
            frequencyMap.put(number, 1);
        } else {
            Integer f = frequencyMap.get(number);
            frequencyMap.put(number, f + 1);
        }
        //  if (HashMap does not contain number) {
        //    Add to HashMap entry: key=number, value=1
        //} else {
        //  Find the entry key=number and increment value by 1
        //}
    }

    private static void printNumbers(HashMap<Integer, Integer> hm) {
        System.out.println("Number | Occurences \n" + "-------+------------");

        System.out.println(hm);

    }

    public static void main(String[] args) throws IOException {
        File file = new File(INPUT_FILE);
        Scanner fileInput = new Scanner(file);
        while (fileInput.hasNextInt()) {
            intList.add(fileInput.nextInt());
        }
        //
        for (int num : intList) {
            //  registerOccurence(num);
            System.out.print(num + ",");
            Integer f = frequencyMap.get(num);
            if (f == null) {
                frequencyMap.put(num, 1);
            } else {
                frequencyMap.put(num, f + 1);
            }
            //        frequencyMap.put(num,f+1);
        }
        fileInput.close();
        printNumbers(frequencyMap);
        //Hash map key does not have to be int it can be string

        //Using Sir Method
        System.out.println("Number | Occurences \n" + "-------+------------");
        for (int key : frequencyMap.keySet()) {
            int value = frequencyMap.get(key);
           // System.out.printf("In %d occured %d times\n" ,key, value);

            System.out.printf("%d | %d \n", key, value);
        }
        //TODO sort by frequency.

    }

}
