package Java1ProgramTasks;

import java.util.Scanner;

public class StaticMethodBinHexOctConversion {

    static String dec2any(int dec, int base) {
        if ((base < 2) || (base > 16)) {
            throw new IllegalArgumentException("Base must be between 2 and 16");
        }
        String [] toHex = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
                            "A", "B", "C", "D", "E", "F"};
        String result = "";
        while (dec > 0) {
            int lastDigit = dec % base;
            String digit = toHex[lastDigit];
            result = digit + result;
            dec = dec / base;
        }
        return result;
    }
    
    static String dec2hex(int dec) {
        String [] toHex = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
                            "A", "B", "C", "D", "E", "F"};
        String result = "";
        while (dec > 0) {
            int lastDigit = dec % 16;
            String hexDigit = toHex[lastDigit];
            result = hexDigit + result;
            dec = dec / 16;
        }
        return result;
    }
    
    static String dec2bin(int dec) {
        return dec2any(dec, 2);
    }
    
    static String dec2oct(int dec) {
        return dec2any(dec, 8);
    }
    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter an positive integer: ");
        int value = input.nextInt();
        
        try {
            System.out.println("In hexadecimal: " + dec2hex(value));
        } catch (IllegalArgumentException e) {
            e.printStackTrace(System.out);
        }
        
        try {
            System.out.println("In binary: " + dec2any(value, 2));
        } catch (IllegalArgumentException e) {
            e.printStackTrace(System.out);
        }
        
        try {
            System.out.println("In octal: " + dec2any(value, 8));
        } catch (IllegalArgumentException e) {
            e.printStackTrace(System.out);
        }
    }
    
}
