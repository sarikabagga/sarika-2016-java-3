package filetwice;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class FileTwice {

    public static void main(String[] args) {
        // Part 1: write one line to text file
        try {
            PrintWriter pw = new PrintWriter("twice.txt");
            pw.println("I hope this works");
            pw.println("Line 2222");
            pw.println("Third 33333333");
            pw.close();
        } catch (IOException e) {
            System.out.println("Error writing to file");
        }
        // Part 2: read the file and display its contents
        File file = new File("twice.txt");
        try {
            Scanner fileInput = new Scanner(file);
            while (fileInput.hasNextLine()) {
                String line = fileInput.nextLine();
                System.out.println(line);
            }
            fileInput.close();
        } catch (IOException e) {
            System.out.println("Error reading from file");
        }
    }
    
}
