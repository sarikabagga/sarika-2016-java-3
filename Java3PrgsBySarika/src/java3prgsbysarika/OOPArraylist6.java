package java3prgsbysarika;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import static java3prgsbysarika.OOPClassFile5.personList;

public class OOPArraylist6 {

    static ArrayList<String> name = new ArrayList<String>();
    //static   ArrayList<Integer> phone = new ArrayList<Integer>();
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        String readString = sc.nextLine();
        String flag1 = "false", flag = "true";
        while (readString != null) {
            //System.out.println(readString);
            if (readString.isEmpty()) {
                //System.out.println("Read Enter Key.");
                flag = "false";
                // System.out.println(flag);
                break;
            }
            if (sc.hasNextLine()) {

                flag1 = "true";
                //System.out.println(flag);
                name.add(readString);
                readString = sc.nextLine();
            } else {
                readString = null;
                flag = "false";
                break;
                //System.out.println(flag);
            }

            // Access and print out the Objects                                          
        }
        if (flag == "false" && flag1 == "true") {
            /*  System.out.println("new size: " + name.size() );
        
             for ( int j=0; j<name.size(); j++ )
             System.out.println("element " + j + ": " + name.get(j) );*/
            System.out.println("Enter file name");
            String filename = sc.nextLine();
            filename = filename + ".txt";
            try {
                PrintWriter pw = new PrintWriter(filename);
                for (int j = 0; j < name.size(); j++) {
                    pw.println(name.get(j));
                }
                pw.close();
                System.out.println("File saved sucessfully");
            } catch (IOException e) {
                System.out.println("Error writing to file");
            }
        }

        /*
         else
         {
         name.add(test);
         }
         */
    }

}
