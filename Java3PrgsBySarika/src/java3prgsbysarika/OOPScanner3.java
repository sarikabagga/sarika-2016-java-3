package java3prgsbysarika;

import java.util.Scanner;

/*      1. Ask user for an integer number and display number back
        2. Ask user for their name and display name back
        3. Are you older than 18 (true/false)?
        4. Ask user for their nationality */
public class OOPScanner3 
{
    static Scanner input = new Scanner(System.in);
    public static void main(String[] args) 
    {
        System.out.print("Give me a nuber: ");
        int number = input.nextInt();
        input.nextLine(); // consume the remaining \n        
        System.out.printf("The number was %d\n", number);
        
        System.out.print("What is your first and last name? ");
        String name = input.nextLine();        
        System.out.printf("Hi %s\n", name);
        
        System.out.print("Are you older than 18 (true/false)? ");
        boolean is18 = input.nextBoolean();
        input.nextLine(); // consume the remaining \n        
        System.out.printf("You are %s\n", is18 ? "an adult" : "a child");
        
        System.out.print("What natinality are you? ");
        String nationality = input.nextLine();
        System.out.printf("You are %s\n", nationality);                    
    }    
}
