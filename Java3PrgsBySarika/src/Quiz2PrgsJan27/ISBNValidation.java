
package Quiz2PrgsJan27;

import java.util.Scanner;

public class ISBNValidation {

    public static void main(String[] args) {
     long isbn;
        int s = 0, i, t, d, dNO;
        String st;
        // Enter the 10-digit ISBN number
           Scanner in = new Scanner(System.in);
            long n;
             System.out.print("Enter the 10-digit ISBN number: ");
            isbn = in.nextLong();
        //InputStreamReader in = new InputStreamReader(System.in);
        //BufferedReader br = new BufferedReader(in);
        //System.out.print("Enter the 10-digit ISBN number: ");
        //isbn = Long.parseLong(br.readLine());
 
        // check the length is 10, else exit the program
        st = "" + isbn;
        if (st.length() != 10) {
            System.out.println("Illegal ISBN");
            return;
        }
        // s=1*1 + 2*4 + 0*0 + 4*1 + 5*6 + 6*0 + 7*1 + 8*4 + 9*9 + 10*9 = 253
        // which is divisible by 11.
        // compute the s of the digits
        s = 0;
        for (i = 0; i < st.length(); i++) {
            d = Integer.parseInt(st.substring(i, i + 1));
            dNO = i + 1;
            t = dNO * d;
            s += t;
        }
 
        // check if divisible by 11
        if ((s % 11) != 0) {
            System.out.println("Illegal ISBN");
        } else {
            System.out.println("Legal ISBN");
        }
    }
}



    
    

