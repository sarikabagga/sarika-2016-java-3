package MagdyPrograms;

import java.util.Scanner;

public class Prime {
	public  static boolean isPrime(int n){
		boolean pr = true;
		for (int i=2 ; i <= Math.floor(n/2)  ; i++)
		if ((n%i)==0)
		pr=false;
		return pr;
	}

public static void main(String args[]){
	int primeTotal=0;
Scanner input = new Scanner(System.in);
System.out.println("Enter a number that would you like to check if it's prime");
int num = input.nextInt();
System.out.println("The number " + num + " is " + (isPrime(num) ? "" : " not " ) + "Prime");

for (int i = 1 ; i < 10000 ; i++){
if (isPrime(i)){
	primeTotal++;
System.out.println("The Prime number " + i + " is Prime" );
}
}
System.out.println("The number of Prime Numbers is: " + primeTotal);
}
}