package FilePrgsBySir;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class FileReadClassArraylist3 {

    static ArrayList<Person> personList = new ArrayList<>();

    public static void main(String[] args) {
        File file = new File("classdata.txt");
        try {
            Scanner fileInput = new Scanner(file);
            while (fileInput.hasNextLine()) {
                String name = fileInput.nextLine();
                int age = fileInput.nextInt();
                fileInput.nextLine(); // consume \n
                Person p = new Person(name, age);
                personList.add(p);
            }
            fileInput.close();
        } catch (IOException e) {
            System.out.println("Error reading from file");
        }
        // TODO: use for-each loop to print all personList object data
        for (Person p : personList) {
            System.out.printf("%s is %d\n", p.getName(), p.getAge());
        }

    }

}

class Person {

    private String Name;
    private int Age;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        if (name.length() < 2) {
            throw new IllegalArgumentException("Name too short");
        }
        this.Name = name;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int age) {
        if ((age < 0) || (age > 150)) {
            throw new IllegalArgumentException("Age must be between 0-150");
        }
        this.Age = age;
    }

    Person(String name, int age) {
        setName(name);
        setAge(age);
    }

}
