
package FilePrgsBySir;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class FileRead2 {

    public static void main(String[] args) {
      // Part 2: read the file and display its contents
        File file = new File("twice1.txt");
        try 
        {
            Scanner fileInput = new Scanner(file);
            while (fileInput.hasNextLine()) 
            {
                String line = fileInput.nextLine();
                System.out.println(line);
            }
            fileInput.close();
        } catch (IOException e)
        {
            System.out.println("Error reading from file");
        }
    }
    
}
