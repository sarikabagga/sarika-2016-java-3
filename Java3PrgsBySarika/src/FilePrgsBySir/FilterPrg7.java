
package FilePrgsBySir;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;


public class FilterPrg7 {
    static ArrayList<Integer> list = new ArrayList<>();
    static ArrayList<Integer> removed = new ArrayList<>();
    static int min, max;

    static void readInput() throws IOException {
        File file = new File("inputfilter.txt");
        Scanner read = new Scanner(file);
        while (read.hasNextInt()) {
            int num = read.nextInt();
            list.add(num);
        }
        read.close();
    }

    static void writeOutput() throws IOException {
        File file = new File("outputfilter.txt");
        File fileRemoved = new File("removedfilter.txt");
        PrintWriter wr = new PrintWriter(file);
        PrintWriter wrRemove = new PrintWriter(fileRemoved);
        //wr.println("maximum: " + max);
        //wr.println("minimum: " + min);
        for (int num : list) {
                wr.println(num);
        }
        
        for (int num : removed) {
                wrRemove.println(num);
        }
        wr.close();
        wrRemove.close();
       
    }

    static void computeResult() {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) < 0 || list.get(i) % 3 == 0) {
                removed.add(list.get(i));
                list.remove(i);
                i--;
            }
        }
        max = Collections.max(list);    
        min = Collections.min(list);
        list.remove(Integer.valueOf(max));
        list.remove(Integer.valueOf(min));
        list.add(0, max);
        list.add(1, min);
        
        Collections.sort(removed);
    }

    public static void main(String[] args) {
        try {
            readInput();
            computeResult();
            writeOutput();
    } 
        catch (IOException e) {
            System.err.println("File access error");
    }
    }
    
}

    

